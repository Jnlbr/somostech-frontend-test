import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './screens/home';
import DetailsScreen from './screens/details';

const navigator = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  Details: {
    screen: DetailsScreen
  }
}, {
  initialRouteName: 'Home'
});

export default createAppContainer(navigator);