import React, { Component } from 'react';
import {
  View,
  FlatList,
  StatusBar
} from 'react-native';
import List from '../../components/list';
import Loading from '../../components/loading';


class Home extends Component {

  state = {
    data: [],
    normalize: {},
    loading: false,
    mounting: true,
  }
  
  async componentDidMount() {
    await this.get();
    this.setState({mounting:false});
  }

  get = async () => {
    this.setState({loading:true});
    const res = await get();
    if(res === null) return;
    const { ordered, normalize } = this.formatData(res.results[0]);
    this.setState({data:ordered, normalize, loading:false});
  }

  formatData(data) {
    const alpha = data.sort((a,b) => a.first < b.first? -1 : 1);
    let ordered = [], byLetter = [], letter = 'A', normalize = {};
    alpha.forEach(al => {
      const startsWith = al.first[0].toUpperCase();
      if(startsWith !== letter) {
        ordered.push({
          key: letter,
          data: byLetter
        });
        byLetter = [];
        letter = startsWith;
      }
      byLetter.push({...al, selected: false });
      normalize[al.email] = {...al, selected: false };
    });

    return { ordered, normalize };
  }

  selectItem = (email) => {
    console.log(email);
    let normalize = this.state.normalize;
    let user = normalize[email];
    user = {
      ...user,
      selected: !user.selected
    }
    normalize[email] = user;
    this.setState({normalize});
  }

  onPressItem = (item) => {
    this.props.navigation.navigate('Details', {item})
  }

  render() {
    return (
      <View style={{ backgroundColor: '#f5f5f7', paddingTop: StatusBar.currentHeight + 5 }}>
        <Loading isVisible={this.state.mounting} />
        <FlatList 
          data={this.state.data}
          renderItem={({item}) => 
            <List 
              label={item.key} 
              data={item.data}
              normalize={this.state.normalize}
              selectItem={this.selectItem}
              onPress={this.onPressItem}
            />}
          onRefresh={this.get}
          refreshing={this.state.loading}
          onPress
        />
      </View>
    );
  }
}


async function get() {
  try {
    const res = await fetch('https://randomapi.com/api/6de6abfedb24f889e0b5f675edc50deb', { method: 'get' });
    return await res.json();
  } catch(err) {
    console.log(err);
    return null;
  }
}

export default Home;