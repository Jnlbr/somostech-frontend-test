import React, { Component, Fragment } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Button,
} from 'react-native';
import { 
  ListItem
} from 'react-native-elements';
import Loading from '../../components/loading';

const SCREEN_HEIGHT = Dimensions.get('window').height;

class Details extends Component {

  constructor(props){
    super(props);
    this.state = {
      item: this.props.navigation.getParam('item'),
      data: {},
      loading: true
    }
  }

  async componentDidMount() {
    const data = await get();
    this.setState({data:data.results[0]});
    this.setState({loading: false});
  }

  render() {
    const { item, data, loading } = this.state;
    
    return (
      <View style={styles.container}>
        {loading? (
          <Loading 
            isVisible={true}
          />
        ) : (
          <Fragment>
            <View style={styles.header}>
              <View style={styles.headerTop}>
                <ListItem 
                  title={`${data.name.first} ${data.name.last}` || `${item.first} ${item.last}`}
                  titleStyle={{
                    color:'white'
                  }}
                  subtitle={data.location.street || item.address}
                  subtitleStyle={{
                    color:'white'
                  }}
                  containerStyle={{
                    backgroundColor:'transparent'
                  }}
                  leftAvatar={{
                    source: {
                      uri: data.picture.medium
                    },
                    size:'medium'
                  }}
                />
              </View>
              <View style={styles.headerBottom}>
              <BottomHeaderItem name="Contacts" value={122}/>
              <BottomHeaderItem name="Favorites" value={40}/>
              <BottomHeaderItem name="Groups" value={8}/>
              </View>
            </View>
            <View style={styles.body}>
              <ListItem 
                title="Add to Favorite"
                containerStyle={{
                  backgroundColor:'transparent'
                }}
                leftIcon={{
                  type:'material',
                  name:'star',
                  color:'#2abab6'
                }}
              />
              <ListItem 
                title={data.phone}
                containerStyle={{
                  backgroundColor:'transparent'
                }}
                leftIcon={{
                  type:'material',
                  name:'phone',
                  color:'#2abab6'
                }}
                rightElement={() => (
                  <Button title="CALL" color="#2abab6" onPress={() => console.log('call')}/>
                )}
              />
              <ListItem 
                title={data.location.city || item.address}
                containerStyle={{
                  backgroundColor:'transparent'
                }}
                leftIcon={{
                  type:'material',
                  name:'location-on',
                  color:'#2abab6'
                }}
              />
            </View>
          </Fragment>
        )}
      </View>
    );
  }
}

const BottomHeaderItem = ({ name, value }) => (
  <View style={{alignItems:'center'}}>
    <Text style={{color:'white'}}>{value}</Text>
    <Text style={{color:'white'}}>{name}</Text>
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f7',
    paddingHorizontal: 7.5,
    paddingTop: 10
  },
  header: {
    // height: SCREEN_HEIGHT * 0.35,
    flex: 2,
    borderRadius: 15,
  },
  headerTop: {
    borderTopStartRadius: 15,
    borderTopEndRadius: 15,
    backgroundColor: '#2abab6',
    justifyContent:'center',
    flex: 3,
  },
  headerBottom: {
    borderBottomStartRadius: 15,
    borderBottomEndRadius: 15,
    backgroundColor: '#2bafad',
    flex: 1,
    flexDirection:'row',
    justifyContent: 'space-between',
    paddingHorizontal: 25,
    paddingVertical: 5,
    alignItems:'center'
  },
  body: {
    flex: 3,
    marginTop: 10,
    backgroundColor: 'white',
    borderRadius: 15,
  }
});


async function get() {
  try {
    const res = await fetch('https://randomuser.me/api/', { method: 'get'});
    return await res.json();
  } catch(err) {
    return null;
  }
}

export default Details;