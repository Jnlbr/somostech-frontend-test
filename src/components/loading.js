import React from 'react'
import { ActivityIndicator } from 'react-native'
import { Overlay } from 'react-native-elements';

export default ({ isVisible, onBackdropPress }) => (
  <Overlay
    onBackdropPress={onBackdropPress}
    isVisible={isVisible}
    windowBackgroundColor="rgba(255, 255, 255, .5)"
    width="auto"
    height="auto"
  >
    <ActivityIndicator 
      size="large" 
      color="#2abab6"
    />
  </Overlay>
);