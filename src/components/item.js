import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { ListItem, Icon, Avatar } from 'react-native-elements';

export default ({data, selectItem, onPress}) => (
  <TouchableOpacity style={styles.container} onPress={onPress}>
    <View style={styles.left}>
      <Avatar 
        title={data.first[0]}
        rounded
      />
      <View style={styles.text}>
        <Text style={styles.title}>{data.first} {data.last}</Text>
        <Text style={styles.subtitle}>{data.email}</Text>
      </View>
    </View>
    <StarIcon selectItem={selectItem} selected={data.selected}/>
  </TouchableOpacity>
);

const StarIcon = ({selectItem, selected}) => (
  <Icon 
    type='material'
    name='star'
    color={selected? 'black':'gray'}
    onPress={selectItem}
  />
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent:'space-between',
    marginVertical: 15,
    alignItems: 'center',
    marginRight: 15
  },
  left: {
    flexDirection: 'row',
    alignItems:'center'
  },
  text: {
    marginLeft: 7.5
  },
  subtitle: {
    color:'gray',
    fontSize: 10
  },
  divider: {
    // width: '100%',
    height: 1,
    backgroundColor:'gray',
    marginRight: 25
  }
})

{/* <ListItem 
    title={`${data.first} ${data.last}`}
    subtitle={data.email}
    onPress={onPress}
    containerStyle={{
      backgroundColor:'red',
      paddingHorizontal: 5
    }}
    rightIcon={<StarIcon selectItem={selectItem} selected={data.selected}/>}
    containerStyle={{
      elevation: 2.5
    }}
  /> */}