import React from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import Item from './item';

export default ({ label, normalize, data, selectItem, onPress }) => (
  <View style={styles.container}>
    <Text style={styles.label}>{label}</Text>
    <View style={styles.list}>
      {data.map((el,i) => (
        <View key={i}>
          <Item
            data={normalize[el.email]}
            selectItem={() => selectItem(el.email)}
            onPress={onPress}
            
          />
          {(data.length-1 !== i) && <View style={styles.divider} />}
        </View>
      ))}
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 15,
  },
  label: {
    marginVertical: 7.5,
    marginLeft: 5
  },
  list: {
    backgroundColor:'white',
    elevation: 2.5,
    borderRadius: 5,
    paddingLeft: 15
  },
  divider: {
    height: 1,
    backgroundColor:'lightgray',
    marginLeft: 40
  }
});