# Aplicacion para la prueba front-end de Somos Tech

Esta aplicacion fue elaborada con React Native, el cual es un framework que permite desarrollar aplicaciones moviles hibridas utilizando la libreria React, por otra parte se utilizo el SDK de Expo, por lo tanto, para probar la misma tiene que instalar la aplicacion *expo* en su telefono movil.

Para probar la aplicacion puede ingresar en el siguiente link https://expo.io/@jnlbr/somostech-frontend-test y escanear el codigo QR con la aplicacion de expo, o puede probarlo en su computadora siguiendo los siguientes pasos: 


Tiene que nstalar expo-cli en su computador con el siguiente comando: 


    npm install expo-cli -g
    

Si utiliza yarn: 

    yarn add expo-cli -global    


Una vez instalado expo-cli, descargue el repositorio:

    git clone https://Jnlbr@bitbucket.org/Jnlbr/somostech-frontend-test.git


Y utilice los siguientes comandos para poder probar la aplicacion:

    npm install 
    npm start

Si utiliza yarn: 

    yarn install
    yarn start


Este comando mostrara en consola un codigo QR que permitira probar la aplicacion utilizando la aplicacion movil de expo para scanear el mismo.